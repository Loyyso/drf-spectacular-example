openapi: 3.0.3
info:
  title: ''
  version: 0.0.0
paths:
  /api/schema/:
    get:
      operationId: api_schema_retrieve
      description: |-
        OpenApi3 schema for this API. Format can be selected via content negotiation.

        - YAML: application/vnd.oai.openapi
        - JSON: application/vnd.oai.openapi+json
      parameters:
      - in: query
        name: format
        schema:
          type: string
          enum:
          - json
          - yaml
      - in: query
        name: lang
        schema:
          type: string
          enum:
          - af
          - ar
          - ar-dz
          - ast
          - az
          - be
          - bg
          - bn
          - br
          - bs
          - ca
          - cs
          - cy
          - da
          - de
          - dsb
          - el
          - en
          - en-au
          - en-gb
          - eo
          - es
          - es-ar
          - es-co
          - es-mx
          - es-ni
          - es-ve
          - et
          - eu
          - fa
          - fi
          - fr
          - fy
          - ga
          - gd
          - gl
          - he
          - hi
          - hr
          - hsb
          - hu
          - hy
          - ia
          - id
          - ig
          - io
          - is
          - it
          - ja
          - ka
          - kab
          - kk
          - km
          - kn
          - ko
          - ky
          - lb
          - lt
          - lv
          - mk
          - ml
          - mn
          - mr
          - my
          - nb
          - ne
          - nl
          - nn
          - os
          - pa
          - pl
          - pt
          - pt-br
          - ro
          - ru
          - sk
          - sl
          - sq
          - sr
          - sr-latn
          - sv
          - sw
          - ta
          - te
          - tg
          - th
          - tk
          - tr
          - tt
          - udm
          - uk
          - ur
          - uz
          - vi
          - zh-hans
          - zh-hant
      tags:
      - api
      security:
      - cookieAuth: []
      - basicAuth: []
      - {}
      responses:
        '200':
          content:
            application/vnd.oai.openapi:
              schema:
                type: object
                additionalProperties: {}
            application/yaml:
              schema:
                type: object
                additionalProperties: {}
            application/vnd.oai.openapi+json:
              schema:
                type: object
                additionalProperties: {}
            application/json:
              schema:
                type: object
                additionalProperties: {}
          description: ''
  /snippets/:
    get:
      operationId: snippets_list
      description: |-
        This viewset automatically provides `list`, `create`, `retrieve`,
        `update` and `destroy` actions.

        Additionally we also provide an extra `highlight` action.
      tags:
      - snippets
      security:
      - cookieAuth: []
      - basicAuth: []
      - {}
      responses:
        '200':
          content:
            application/json:
              schema:
                type: array
                items:
                  $ref: '#/components/schemas/Snippet'
          description: ''
    post:
      operationId: snippets_create
      description: |-
        This viewset automatically provides `list`, `create`, `retrieve`,
        `update` and `destroy` actions.

        Additionally we also provide an extra `highlight` action.
      tags:
      - snippets
      requestBody:
        content:
          application/json:
            schema:
              $ref: '#/components/schemas/Snippet'
          application/x-www-form-urlencoded:
            schema:
              $ref: '#/components/schemas/Snippet'
          multipart/form-data:
            schema:
              $ref: '#/components/schemas/Snippet'
        required: true
      security:
      - cookieAuth: []
      - basicAuth: []
      - {}
      responses:
        '200':
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/Snippet'
          description: ''
  /snippets/{id}/:
    get:
      operationId: snippets_retrieve
      description: |-
        This viewset automatically provides `list`, `create`, `retrieve`,
        `update` and `destroy` actions.

        Additionally we also provide an extra `highlight` action.
      parameters:
      - in: path
        name: id
        schema:
          type: integer
        description: A unique integer value identifying this snippet.
        required: true
      tags:
      - snippets
      security:
      - cookieAuth: []
      - basicAuth: []
      - {}
      responses:
        '200':
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/Snippet'
          description: ''
    put:
      operationId: snippets_update
      description: |-
        This viewset automatically provides `list`, `create`, `retrieve`,
        `update` and `destroy` actions.

        Additionally we also provide an extra `highlight` action.
      parameters:
      - in: path
        name: id
        schema:
          type: integer
        description: A unique integer value identifying this snippet.
        required: true
      tags:
      - snippets
      requestBody:
        content:
          application/json:
            schema:
              $ref: '#/components/schemas/Snippet'
          application/x-www-form-urlencoded:
            schema:
              $ref: '#/components/schemas/Snippet'
          multipart/form-data:
            schema:
              $ref: '#/components/schemas/Snippet'
        required: true
      security:
      - cookieAuth: []
      - basicAuth: []
      - {}
      responses:
        '200':
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/Snippet'
          description: ''
    patch:
      operationId: snippets_partial_update
      description: |-
        This viewset automatically provides `list`, `create`, `retrieve`,
        `update` and `destroy` actions.

        Additionally we also provide an extra `highlight` action.
      parameters:
      - in: path
        name: id
        schema:
          type: integer
        description: A unique integer value identifying this snippet.
        required: true
      tags:
      - snippets
      requestBody:
        content:
          application/json:
            schema:
              $ref: '#/components/schemas/PatchedSnippet'
          application/x-www-form-urlencoded:
            schema:
              $ref: '#/components/schemas/PatchedSnippet'
          multipart/form-data:
            schema:
              $ref: '#/components/schemas/PatchedSnippet'
      security:
      - cookieAuth: []
      - basicAuth: []
      - {}
      responses:
        '200':
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/Snippet'
          description: ''
    delete:
      operationId: snippets_destroy
      description: |-
        This viewset automatically provides `list`, `create`, `retrieve`,
        `update` and `destroy` actions.

        Additionally we also provide an extra `highlight` action.
      parameters:
      - in: path
        name: id
        schema:
          type: integer
        description: A unique integer value identifying this snippet.
        required: true
      tags:
      - snippets
      security:
      - cookieAuth: []
      - basicAuth: []
      - {}
      responses:
        '204':
          description: No response body
  /snippets/{id}/highlight/:
    get:
      operationId: snippets_highlight_retrieve
      description: |-
        This viewset automatically provides `list`, `create`, `retrieve`,
        `update` and `destroy` actions.

        Additionally we also provide an extra `highlight` action.
      parameters:
      - in: path
        name: id
        schema:
          type: integer
        description: A unique integer value identifying this snippet.
        required: true
      tags:
      - snippets
      security:
      - cookieAuth: []
      - basicAuth: []
      - {}
      responses:
        '200':
          content:
            text/html:
              schema:
                $ref: '#/components/schemas/Snippet'
          description: ''
components:
  schemas:
    LanguageEnum:
      enum:
      - abap
      - abnf
      - ada
      - adl
      - agda
      - aheui
      - ahk
      - alloy
      - ampl
      - antlr
      - antlr-as
      - antlr-cpp
      - antlr-csharp
      - antlr-java
      - antlr-objc
      - antlr-perl
      - antlr-python
      - antlr-ruby
      - apacheconf
      - apl
      - applescript
      - arduino
      - arrow
      - as
      - as3
      - aspectj
      - aspx-cs
      - aspx-vb
      - asy
      - at
      - augeas
      - autoit
      - awk
      - bare
      - basemake
      - bash
      - bat
      - bbcbasic
      - bbcode
      - bc
      - befunge
      - bib
      - blitzbasic
      - blitzmax
      - bnf
      - boa
      - boo
      - boogie
      - brainfuck
      - bst
      - bugs
      - c
      - c-objdump
      - ca65
      - cadl
      - camkes
      - capdl
      - capnp
      - cbmbas
      - ceylon
      - cfc
      - cfengine3
      - cfm
      - cfs
      - chai
      - chapel
      - charmci
      - cheetah
      - cirru
      - clay
      - clean
      - clojure
      - clojurescript
      - cmake
      - cobol
      - cobolfree
      - coffee-script
      - common-lisp
      - componentpascal
      - console
      - control
      - coq
      - cpp
      - cpp-objdump
      - cpsa
      - cr
      - crmsh
      - croc
      - cryptol
      - csharp
      - csound
      - csound-document
      - csound-score
      - css
      - css+django
      - css+erb
      - css+genshitext
      - css+lasso
      - css+mako
      - css+mozpreproc
      - css+myghty
      - css+php
      - css+smarty
      - cucumber
      - cuda
      - cypher
      - cython
      - d
      - d-objdump
      - dart
      - dasm16
      - delphi
      - devicetree
      - dg
      - diff
      - django
      - docker
      - doscon
      - dpatch
      - dtd
      - duel
      - dylan
      - dylan-console
      - dylan-lid
      - earl-grey
      - easytrieve
      - ebnf
      - ec
      - ecl
      - eiffel
      - elixir
      - elm
      - emacs
      - email
      - erb
      - erl
      - erlang
      - evoque
      - execline
      - extempore
      - ezhil
      - factor
      - fan
      - fancy
      - felix
      - fennel
      - fish
      - flatline
      - floscript
      - forth
      - fortran
      - fortranfixed
      - foxpro
      - freefem
      - fsharp
      - fstar
      - gap
      - gas
      - gdscript
      - genshi
      - genshitext
      - glsl
      - gnuplot
      - go
      - golo
      - gooddata-cl
      - gosu
      - groff
      - groovy
      - gst
      - haml
      - handlebars
      - haskell
      - haxeml
      - hexdump
      - hlsl
      - hsail
      - hspec
      - html
      - html+cheetah
      - html+django
      - html+evoque
      - html+genshi
      - html+handlebars
      - html+lasso
      - html+mako
      - html+myghty
      - html+ng2
      - html+php
      - html+smarty
      - html+twig
      - html+velocity
      - http
      - hx
      - hybris
      - hylang
      - i6t
      - icon
      - idl
      - idris
      - iex
      - igor
      - inform6
      - inform7
      - ini
      - io
      - ioke
      - irc
      - isabelle
      - j
      - jags
      - jasmin
      - java
      - javascript+mozpreproc
      - jcl
      - jlcon
      - js
      - js+cheetah
      - js+django
      - js+erb
      - js+genshitext
      - js+lasso
      - js+mako
      - js+myghty
      - js+php
      - js+smarty
      - jsgf
      - json
      - json-object
      - jsonld
      - jsp
      - julia
      - juttle
      - kal
      - kconfig
      - kmsg
      - koka
      - kotlin
      - lagda
      - lasso
      - lcry
      - lean
      - less
      - lhs
      - lidr
      - lighty
      - limbo
      - liquid
      - live-script
      - llvm
      - llvm-mir
      - llvm-mir-body
      - logos
      - logtalk
      - lsl
      - lua
      - make
      - mako
      - maql
      - mask
      - mason
      - mathematica
      - matlab
      - matlabsession
      - md
      - mime
      - minid
      - modelica
      - modula2
      - monkey
      - monte
      - moocode
      - moon
      - mosel
      - mozhashpreproc
      - mozpercentpreproc
      - mql
      - ms
      - mscgen
      - mupad
      - mxml
      - myghty
      - mysql
      - nasm
      - ncl
      - nemerle
      - nesc
      - newlisp
      - newspeak
      - ng2
      - nginx
      - nim
      - nit
      - nixos
      - notmuch
      - nsis
      - numpy
      - nusmv
      - objdump
      - objdump-nasm
      - objective-c
      - objective-c++
      - objective-j
      - ocaml
      - octave
      - odin
      - ooc
      - opa
      - openedge
      - pacmanconf
      - pan
      - parasail
      - pawn
      - peg
      - perl
      - perl6
      - php
      - pig
      - pike
      - pkgconfig
      - plpgsql
      - pointless
      - pony
      - postgresql
      - postscript
      - pot
      - pov
      - powershell
      - praat
      - prolog
      - promql
      - properties
      - protobuf
      - ps1con
      - psql
      - psysh
      - pug
      - puppet
      - py2tb
      - pycon
      - pypylog
      - pytb
      - python
      - python2
      - qbasic
      - qml
      - qvto
      - racket
      - ragel
      - ragel-c
      - ragel-cpp
      - ragel-d
      - ragel-em
      - ragel-java
      - ragel-objc
      - ragel-ruby
      - raw
      - rb
      - rbcon
      - rconsole
      - rd
      - reason
      - rebol
      - red
      - redcode
      - registry
      - resource
      - rexx
      - rhtml
      - ride
      - rnc
      - roboconf-graph
      - roboconf-instances
      - robotframework
      - rql
      - rsl
      - rst
      - rts
      - rust
      - sarl
      - sas
      - sass
      - sc
      - scala
      - scaml
      - scdoc
      - scheme
      - scilab
      - scss
      - sgf
      - shen
      - shexc
      - sieve
      - silver
      - singularity
      - slash
      - slim
      - slurm
      - smali
      - smalltalk
      - smarty
      - sml
      - snobol
      - snowball
      - solidity
      - sourceslist
      - sp
      - sparql
      - spec
      - splus
      - sql
      - sqlite3
      - squidconf
      - ssp
      - stan
      - stata
      - swift
      - swig
      - systemverilog
      - tads3
      - tap
      - tasm
      - tcl
      - tcsh
      - tcshcon
      - tea
      - termcap
      - terminfo
      - terraform
      - tex
      - text
      - thrift
      - tid
      - tnt
      - todotxt
      - toml
      - trac-wiki
      - treetop
      - ts
      - tsql
      - ttl
      - turtle
      - twig
      - typoscript
      - typoscriptcssdata
      - typoscripthtmldata
      - ucode
      - unicon
      - urbiscript
      - usd
      - vala
      - vb.net
      - vbscript
      - vcl
      - vclsnippets
      - vctreestatus
      - velocity
      - verilog
      - vgl
      - vhdl
      - vim
      - wdiff
      - webidl
      - whiley
      - x10
      - xml
      - xml+cheetah
      - xml+django
      - xml+erb
      - xml+evoque
      - xml+lasso
      - xml+mako
      - xml+myghty
      - xml+php
      - xml+smarty
      - xml+velocity
      - xorg.conf
      - xquery
      - xslt
      - xtend
      - xul+mozpreproc
      - yaml
      - yaml+jinja
      - yang
      - zeek
      - zephir
      - zig
      type: string
    PatchedSnippet:
      type: object
      properties:
        id:
          type: integer
          readOnly: true
        title:
          type: string
          maxLength: 100
        code:
          type: string
        linenos:
          type: boolean
        language:
          $ref: '#/components/schemas/LanguageEnum'
        style:
          $ref: '#/components/schemas/StyleEnum'
    Snippet:
      type: object
      properties:
        id:
          type: integer
          readOnly: true
        title:
          type: string
          maxLength: 100
        code:
          type: string
        linenos:
          type: boolean
        language:
          $ref: '#/components/schemas/LanguageEnum'
        style:
          $ref: '#/components/schemas/StyleEnum'
      required:
      - code
      - id
    StyleEnum:
      enum:
      - abap
      - algol
      - algol_nu
      - arduino
      - autumn
      - borland
      - bw
      - colorful
      - default
      - emacs
      - friendly
      - fruity
      - igor
      - inkpot
      - lovelace
      - manni
      - monokai
      - murphy
      - native
      - paraiso-dark
      - paraiso-light
      - pastie
      - perldoc
      - rainbow_dash
      - rrt
      - sas
      - solarized-dark
      - solarized-light
      - stata
      - stata-dark
      - stata-light
      - tango
      - trac
      - vim
      - vs
      - xcode
      type: string
  securitySchemes:
    basicAuth:
      type: http
      scheme: basic
    cookieAuth:
      type: apiKey
      in: cookie
      name: Session
